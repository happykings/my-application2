package com.happyking.leobots.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.model.AddNoteResponse;
import com.happyking.leobots.myapplication.model.GetNoteResponse;
import com.happyking.leobots.myapplication.model.LoginResponse;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;
import com.happyking.leobots.myapplication.viewModel.AddNoteModel;
import com.happyking.leobots.myapplication.viewModel.LoginModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.happyking.leobots.myapplication.Constants.TAG;



    public class AddNoteFragment extends Fragment  implements View.OnClickListener{

        private AppCompatButton btn_upload;
        private EditText et_localid,et_noteid,et_notetitle,et_notedescription,et_addeddate;

        private ProgressBar progress;
        private SharedPrefUtil pre;
        private AddNoteModel Anm;


       

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_add_note,container,false);
            try {
                initViews(view);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return view;
        }

        private void initViews(View view) throws ParseException {
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy ");
            String dateToStr = format.format(today);


            btn_upload = (AppCompatButton)view.findViewById(R.id.btn_upload);
            et_localid = (EditText)view.findViewById(R.id.et_localid);
            et_noteid = (EditText)view.findViewById(R.id.et_noteid);
            et_notetitle = (EditText)view.findViewById(R.id.et_notetitle);
            et_notedescription=(EditText)view.findViewById(R.id.et_notedescription);
            et_addeddate = (EditText)view.findViewById(R.id.et_addeddate);
            et_addeddate.setText(dateToStr);


            progress = (ProgressBar)view.findViewById(R.id.progress);

            btn_upload.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case R.id.tv_login:

                    break;

                case R.id.btn_upload:

                    String localid = et_localid.getText().toString();
                    String noteid = et_noteid.getText().toString();
                    String notetitle= et_notetitle.getText().toString();
                    String notedescription=et_notedescription.getText().toString();
                    String addeddate=et_addeddate.getText().toString();



                    if(!localid.isEmpty() && !noteid.isEmpty() && !notetitle.isEmpty()&& !notedescription.isEmpty()&& !addeddate.isEmpty()) {

                        progress.setVisibility(View.VISIBLE);
                        UploadNoteProcess(localid,noteid,notetitle,notedescription,addeddate);

                    } else {

                        Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                    }
                    break;

            }

        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            // init ViewModel
            Anm= ViewModelProviders.of(getActivity()).get(AddNoteModel.class);
            Anm .init();
            Anm.addNoteResponse().observe(this, new Observer<AddNoteResponse>() {
                @Override
                public void onChanged( AddNoteResponse addNoteResponse) {
                    progress.setVisibility(View.INVISIBLE);


                    if(addNoteResponse.getStatus().getCode()==Constants.STATUS_SUCCESS_CODE)

                    {

                        Snackbar.make(getView(), addNoteResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                   /* SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Constants.IS_LOGGED_IN,true);
                    editor.putString(Constants.EMAIL,resp.getSession().getEmail());
                    editor.putString(Constants.MOBILE,resp.getSession().getMobile());

                   // editor.putString(Constants.NAME,resp.getUser().getName());
                    editor.putString(Constants.device_id,resp.getSession().getDevice_id());
                    editor.putString(Constants.Authorizations,resp.getSession().getAuthorizations());
                    editor.putString(Constants.refresh_token,resp.getSession().getRefresh_token());
                    editor.apply();

                    */

                        // pre.createLoginSession(loginResponse.getSession().getMobile(),loginResponse.getSession().getEmail(),loginResponse.getSession().getAuthorizations(),loginResponse.getSession().getRefresh_token(),loginResponse.getSession().getDevice_id());

                        Log.e(TAG, "onResponse: "+addNoteResponse.getStatus().getMessage() );
                        Log.e(TAG, "onResponse: "+addNoteResponse.getStatus().getCode() );








                    }
                    else if(addNoteResponse.getStatus().getCode() == Constants.STATUS_SESSION_CODE)
                    {
                        Snackbar.make(getView(), addNoteResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                        goToLogin();
                    }
                    else
                    {
                        Snackbar.make(getView(), addNoteResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                    }

                }
            });

        }

        private void UploadNoteProcess(String localid,String noteid,String notetitle,String notedescription,String addeddate){
            pre = new SharedPrefUtil(getContext());
            Map<String, String> params = new HashMap<String, String>();
            params.put("Authorizations",pre.getKey_Authorizations());

            Map<String, String> params1 = new HashMap<String, String>();
            params1.put ("localid", localid);
            params1.put ("noteid", noteid);
            params1.put ("notetitle", notetitle);
            params1.put ("notedescription", notedescription);
            params1.put("addeddate",addeddate);

            Anm.addnotes(params,params1);


        }

        private void goToLogin(){

            Fragment login = new LoginFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_frame,login);
            ft.commit();


        }

}
