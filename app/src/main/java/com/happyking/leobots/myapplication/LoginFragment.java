package com.happyking.leobots.myapplication;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentTransaction;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.model.LoginResponse;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;
import com.happyking.leobots.myapplication.viewModel.LoginModel;

import java.util.HashMap;
import java.util.Map;

import static com.happyking.leobots.myapplication.Constants.TAG;


public class LoginFragment extends Fragment implements View.OnClickListener {

    private AppCompatButton btn_login;
    private EditText et_email,et_password;
    private TextView tv_register;
    private ProgressBar progress;
    private SharedPrefUtil pre;
    private LoginModel lvm;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login,container,false);
        initViews(view);


        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // init ViewModel
        lvm= ViewModelProviders.of(getActivity()).get(LoginModel.class);
        lvm .init();
        lvm.getLoginResponse().observe(this, new Observer<LoginResponse>() {
            @Override
            public void onChanged( LoginResponse loginResponse) {
                progress.setVisibility(View.INVISIBLE);

                if(loginResponse.getStatus().getCode()==Constants.STATUS_SUCCESS_CODE)
                {

                    Snackbar.make(getView(), loginResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                   /* SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Constants.IS_LOGGED_IN,true);
                    editor.putString(Constants.EMAIL,resp.getSession().getEmail());
                    editor.putString(Constants.MOBILE,resp.getSession().getMobile());

                   // editor.putString(Constants.NAME,resp.getUser().getName());
                    editor.putString(Constants.device_id,resp.getSession().getDevice_id());
                    editor.putString(Constants.Authorizations,resp.getSession().getAuthorizations());
                    editor.putString(Constants.refresh_token,resp.getSession().getRefresh_token());
                    editor.apply();

                    */

                    pre.createLoginSession(loginResponse.getSession().getMobile(),loginResponse.getSession().getEmail(),loginResponse.getSession().getAuthorizations(),loginResponse.getSession().getRefresh_token(),loginResponse.getSession().getDevice_id());

                    Log.e(TAG, "onResponse: "+loginResponse.getStatus().getMessage() );
                    Log.e(TAG, "onResponse: "+loginResponse.getStatus().getCode() );
                    Log.e(TAG, "onResponse: "+pre.getKey_Authorizations() );
                    Log.e(TAG, "onResponse: "+loginResponse.getSession().getEmail() );


                    goToProfile();





                }
                else
                    Snackbar.make(getView(), loginResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

            }

        });

    }

    private void initViews(View view){

        pre=new SharedPrefUtil(getActivity());

        btn_login = (AppCompatButton)view.findViewById(R.id.btn_login);
        tv_register = (TextView)view.findViewById(R.id.tv_register);
        et_email = (EditText)view.findViewById(R.id.et_email);
        et_password = (EditText)view.findViewById(R.id.et_password);

        progress = (ProgressBar)view.findViewById(R.id.progress);

        btn_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_register:
                goToRegister();
                break;

            case R.id.btn_login:
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                if(!email.isEmpty() && !password.isEmpty()) {

                    progress.setVisibility(View.VISIBLE);
                    loginProcess(email,password);

                } else {

                    Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                }
                break;

        }
    }

    private void loginProcess(String email, String password){
        Map<String, String> params = new HashMap<String, String>();
        params.put ("emailormobile", email);
        params.put ("password", password);
        params.put ("device_id", Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID ));
        lvm.doLogin(params);
    }

    private void goToRegister(){

       Fragment register = new RegisterFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,register);
        ft.commit();
    }

    private void goToProfile(){

       Fragment profile = new ProfileFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,profile);
        ft.commit();
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
