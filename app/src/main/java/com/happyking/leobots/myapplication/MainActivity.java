package com.happyking.leobots.myapplication;

import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;

public class MainActivity extends AppCompatActivity {

    private SharedPrefUtil pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = new SharedPrefUtil(getApplicationContext());
        initFragment();
    }

    private void initFragment(){
        Fragment fragment;
        if(pref.getIsLogin()){
           fragment = new ProfileFragment();
        }else {
            fragment = new LoginFragment();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,fragment);
        ft.commit();
    }

}
