package com.happyking.leobots.myapplication;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;
import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.adapter.NoteAdapter;
import com.happyking.leobots.myapplication.model.AddData;
import com.happyking.leobots.myapplication.model.AddNoteResponse;
import com.happyking.leobots.myapplication.model.GetData;
import com.happyking.leobots.myapplication.model.GetNoteResponse;
import com.happyking.leobots.myapplication.model.Note;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;
import com.happyking.leobots.myapplication.viewModel.AddNoteModel;
import com.happyking.leobots.myapplication.viewModel.GetNoteModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NoteFragment extends Fragment {
    private SharedPrefUtil pref;
    ArrayList<Note> articleArrayList = new ArrayList<>();
    NoteAdapter noteAdapter;
    RecyclerView rvNote;
    GetNoteModel noteViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);
        noteViewModel = ViewModelProviders.of(getActivity()).get(GetNoteModel.class);
        noteViewModel.init();
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        pref = new SharedPrefUtil(getContext());

        Map<String, String> params = new HashMap<String, String>();
        params.put("Authorizations",pref.getKey_Authorizations());

        noteViewModel.getnotes(params);



    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    private void initViews(View view) {

        rvNote = view.findViewById(R.id.rvNote);


        noteViewModel.getNoteResponse().observe(this, new Observer<GetNoteResponse>() {
            @Override
            public void onChanged(GetNoteResponse noteResponse) {
                if (noteResponse.getStatus().getCode() == Constants.STATUS_SUCCESS_CODE)
                    {
                        List<Note> notedetails = (List<Note>) noteResponse.getData().getNote();
                        articleArrayList.addAll(notedetails);
                        noteAdapter.notifyDataSetChanged();
                    }
                    else if(noteResponse.getStatus().getCode() == Constants.STATUS_SESSION_CODE)
                    {
                        Snackbar.make(getView(), noteResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                        goToLogin();
                    }
                    else
                {
                    Snackbar.make(getView(), noteResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                }



            }
        });

        setupRecyclerView();
    }


    /*
    setContentView(R.layout.fragment_note);
        rvNote = findViewById(R.id.rvNote);

        noteViewModel = ViewModelProviders.of(getActivity()).get(AddNoteModel.class);
        noteViewModel.init();
        noteViewModel.getNoteResponse().observe(this, new Observer<AddNoteResponse>() {
            @Override
            public void onChanged(AddNoteResponse noteResponse) {
                List<AddData> notedetails = noteResponse.getData();
                articleArrayList.addAll(notedetails);
                noteAdapter.notifyDataSetChanged();
            }
        });

        setupRecyclerView();
private void setupRecyclerView() {
        if (noteAdapter == null) {
            noteAdapter = new NoteAdapter(getActivity(), articleArrayList);
            rvNote.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvNote.setAdapter(noteAdapter);
            rvNote.setItemAnimator(new DefaultItemAnimator());
            rvNote.setNestedScrollingEnabled(true);
        } else {
            noteAdapter.notifyDataSetChanged();
        }

     */
    private void setupRecyclerView() {
        if (noteAdapter == null) {
            noteAdapter = new NoteAdapter(getActivity(), articleArrayList);
            rvNote.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvNote.setAdapter(noteAdapter);
            rvNote.setItemAnimator(new DefaultItemAnimator());
            rvNote.setNestedScrollingEnabled(true);
        } else {
            noteAdapter.notifyDataSetChanged();
        }


    }
    private void goToLogin(){

        Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,login);
        ft.commit();


    }

}




