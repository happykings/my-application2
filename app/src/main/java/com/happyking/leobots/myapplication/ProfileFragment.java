package com.happyking.leobots.myapplication;

import android.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentTransaction;

import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private TextView tv_name,tv_email,tv_message;
    private SharedPrefUtil pref;
    private AppCompatButton Btview_note,btn_logout,bt_add_Note;
    private EditText et_old_password,et_new_password;
    private AlertDialog dialog;
    private ProgressBar progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        pref = new SharedPrefUtil(getActivity());
        tv_name.setText("Welcome : "+pref.getKeyMobile());
        tv_email.setText(pref.getKeyEmail());

    }

    private void initViews(View view){

        tv_name = (TextView)view.findViewById(R.id.tv_name);
        tv_email = (TextView)view.findViewById(R.id.tv_email);
        btn_logout = (AppCompatButton)view.findViewById(R.id.btn_logout);
        bt_add_Note=(AppCompatButton)view.findViewById(R.id.bt_add_Note);
        Btview_note=(AppCompatButton)view.findViewById(R.id.Btview_note);

        btn_logout.setOnClickListener(this);
        bt_add_Note.setOnClickListener(this);
        Btview_note.setOnClickListener(this);

    }

    private void showDialog(){


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


            case R.id.btn_logout:
                logout();
                break;
            case R.id.bt_add_Note:
                goToAddNote();
                break;
            case R.id.Btview_note:
                goToViewNote();
                break;
        }
    }

    private void logout() {
        pref.logoutUser();
        goToLogin();
    }

    private void goToLogin(){

        Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,login);
        ft.commit();
    }

    private void goToAddNote()
    {
        Fragment addnote = new AddNoteFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,addnote);
        ft.commit();
    }

    private void goToViewNote()
    {
        Fragment note = new NoteFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,note);
        ft.commit();
    }

   /* private void changePasswordProcess(String email,String old_password,String new_password){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestApi requestInterface = retrofit.create(RequestApi.class);

        User user = new User();
        user.setEmail(email);
     //   user.setOld_password(old_password);
      //  user.setNew_password(new_password);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.CHANGE_PASSWORD_OPERATION);
        request.setUser(user);
        Call<LoginResponse> response = requestInterface.operation(request);

        response.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {

                LoginResponse resp = response.body();
                if(resp.getResult().equals(Constants.SUCCESS)){
                    progress.setVisibility(View.GONE);
                    tv_message.setVisibility(View.GONE);
                    dialog.dismiss();
                    Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();

                }else {
                    progress.setVisibility(View.GONE);
                    tv_message.setVisibility(View.VISIBLE);
                    tv_message.setText(resp.getMessage());

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Log.d(Constants.TAG,"failed");
                progress.setVisibility(View.GONE);
                tv_message.setVisibility(View.VISIBLE);
                tv_message.setText(t.getLocalizedMessage());


            }
        });
    }*/
}