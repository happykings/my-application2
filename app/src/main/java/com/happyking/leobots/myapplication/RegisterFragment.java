package com.happyking.leobots.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.model.LoginResponse;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;
import com.happyking.leobots.myapplication.viewModel.LoginModel;

import java.util.HashMap;
import java.util.Map;

import static com.happyking.leobots.myapplication.Constants.TAG;


public class RegisterFragment extends Fragment  implements View.OnClickListener{

    private AppCompatButton btn_register;
    private EditText et_email,et_password,et_mobile;
    private TextView tv_login;
    private ProgressBar progress;
    private SharedPrefUtil pre;
    private LoginModel lvm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View view){

        btn_register = (AppCompatButton)view.findViewById(R.id.btn_register);
        tv_login = (TextView)view.findViewById(R.id.tv_login);
        et_mobile = (EditText)view.findViewById(R.id.et_mobile);
        et_email = (EditText)view.findViewById(R.id.et_email);
        et_password = (EditText)view.findViewById(R.id.et_password);

        progress = (ProgressBar)view.findViewById(R.id.progress);

        btn_register.setOnClickListener(this);
        tv_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_login:
                goToLogin();
                break;

            case R.id.btn_register:

                String mobile = et_mobile.getText().toString();
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                if(!mobile.isEmpty() && !email.isEmpty() && !password.isEmpty()) {

                    progress.setVisibility(View.VISIBLE);
                    registerProcess(mobile,email,password);

                } else {

                    Snackbar.make(getView(), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                }
                break;

        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // init ViewModel
        lvm= ViewModelProviders.of(getActivity()).get(LoginModel.class);
        lvm .init();
        lvm.getLoginResponse().observe(this, new Observer<LoginResponse>() {
            @Override
            public void onChanged( LoginResponse loginResponse) {
                progress.setVisibility(View.INVISIBLE);


                if(loginResponse.getStatus().getCode()==Constants.STATUS_SUCCESS_CODE)

                {
                   // lvm.getLoginResponse().removeObserver(this);
                    Snackbar.make(getView(), loginResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

                   /* SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(Constants.IS_LOGGED_IN,true);
                    editor.putString(Constants.EMAIL,resp.getSession().getEmail());
                    editor.putString(Constants.MOBILE,resp.getSession().getMobile());

                   // editor.putString(Constants.NAME,resp.getUser().getName());
                    editor.putString(Constants.device_id,resp.getSession().getDevice_id());
                    editor.putString(Constants.Authorizations,resp.getSession().getAuthorizations());
                    editor.putString(Constants.refresh_token,resp.getSession().getRefresh_token());
                    editor.apply();

                    */

                   // pre.createLoginSession(loginResponse.getSession().getMobile(),loginResponse.getSession().getEmail(),loginResponse.getSession().getAuthorizations(),loginResponse.getSession().getRefresh_token(),loginResponse.getSession().getDevice_id());

                    Log.e(TAG, "onResponse: "+loginResponse.getStatus().getMessage() );
                    Log.e(TAG, "onResponse: "+loginResponse.getStatus().getCode() );
                    Log.e(TAG, "onResponse: "+loginResponse.getSession().getAuthorizations() );
                    Log.e(TAG, "onResponse: "+loginResponse.getSession().getEmail() );


                    goToLogin();





                }
                else
                    Snackbar.make(getView(), loginResponse.getStatus().getMessage(), Snackbar.LENGTH_LONG).show();

            }
        });

    }

    private void registerProcess(String mobile, String email,String password){
        Map<String, String> params = new HashMap<String, String>();
        params.put ("email", email);
        params.put ("mobile", mobile);
        params.put ("password", password);
        params.put ("device_id", Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID ));
        lvm.register(params);


    }

    private void goToLogin(){

       Fragment login = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,login);
        ft.commit();


    }


}
