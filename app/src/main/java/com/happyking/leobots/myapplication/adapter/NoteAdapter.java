package com.happyking.leobots.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.happyking.leobots.ChipsyMVVM.R;
import com.happyking.leobots.myapplication.model.AddData;
import com.happyking.leobots.myapplication.model.GetData;
import com.happyking.leobots.myapplication.model.Note;

import java.util.ArrayList;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    Context context;
    ArrayList<Note> notes;

    public NoteAdapter(Context context, ArrayList<Note> notes) {
        this.context = context;
        this.notes = notes;
    }

    @NonNull
    @Override
    public NoteAdapter.NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.note_item, parent, false);
        return new  NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteAdapter.NoteViewHolder holder, int position) {
        holder.tvNoteId.setText(String.valueOf(notes.get(position).getNoteid()));
        holder.tvNoteTitle.setText(notes.get(position).getNotetitle());
        holder.tvNoteDescription.setText(notes.get(position).getNotedescription());
        holder.tvAddedDate.setText(notes.get(position).getAddeddate());
        holder.tvUpdateDdate.setText(notes.get(position).getUpdateddate());
    }


    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder{

        TextView tvNoteId;
        TextView tvNoteTitle;
        TextView tvNoteDescription;
        TextView tvAddedDate;
        TextView tvUpdateDdate;






        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNoteId = itemView.findViewById(R.id.tvNoteId);
            tvNoteTitle = itemView.findViewById(R.id.tvNoteTitle);
            tvNoteDescription = itemView.findViewById(R.id.tvNoteDescription);
            tvAddedDate = itemView.findViewById(R.id.tvAddedDate);
            tvUpdateDdate = itemView.findViewById(R.id.tvUpdateDdate);

        }
    }

}
