package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class AddData {
    @SerializedName("action")
    private String action;

    @SerializedName("noteid")
    private float noteid;

    @SerializedName("localid")
    private String localid;

    @SerializedName("notetitle")
    private String notetitle;

    @SerializedName("notedescription")
    private String notedescription;

    @SerializedName("addeddate")
    private String addeddate;

    @SerializedName("updateddate")
    private String updateddate;


    // Getter Methods

    public String getAction() {
        return action;
    }

    public float getNoteid() {
        return noteid;
    }

    public String getLocalid() {
        return localid;
    }

    public String getNotetitle() {
        return notetitle;
    }

    public String getNotedescription() {
        return notedescription;
    }

    public String getAddeddate() {
        return addeddate;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    // Setter Methods

    public void setAction(String action) {
        this.action = action;
    }

    public void setNoteid(float noteid) {
        this.noteid = noteid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public void setNotetitle(String notetitle) {
        this.notetitle = notetitle;
    }

    public void setNotedescription(String notedescription) {
        this.notedescription = notedescription;
    }

    public void setAddeddate(String addeddate) {
        this.addeddate = addeddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }
}
