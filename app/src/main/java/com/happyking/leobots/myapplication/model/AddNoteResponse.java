package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class AddNoteResponse {
    @SerializedName("status")
    Status status;

    @SerializedName("data")
    AddData data;


    // Getter Methods

    public Status getStatus() {
        return status;
    }

    public AddData getData() {
        return data;
    }

    // Setter Methods

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setData(AddData addData) {
        this.data = addData;
    }
}