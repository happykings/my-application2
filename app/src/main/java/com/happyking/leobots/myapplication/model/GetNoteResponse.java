package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class GetNoteResponse {
    @SerializedName("status")
    Status status;

    @SerializedName("data")
    GetData data;


    // Getter Methods

    public Status getStatus() {
        return status;
    }

    public GetData getData() {
        return data;
    }

    // Setter Methods

    public void setStatus(Status statusObject) {
        this.status = statusObject;
    }

    public void setData(GetData dataObject) {
        this.data = dataObject;
    }


}
