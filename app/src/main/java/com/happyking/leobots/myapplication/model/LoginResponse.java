package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse  {

    @SerializedName("status")
    @Expose

    Status status;

    @SerializedName("session")


    Session session;


    // Getter Methods

    public Status getStatus() {
        return status;
    }

    public Session getSession() {
        return session;
    }

    // Setter Methods

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setSession(Session sessionObject) {
        this.session = sessionObject;
    }
}
