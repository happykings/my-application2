package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class Note {
    public float getNoteid() {
        return noteid;
    }

    public void setNoteid(float noteid) {
        this.noteid = noteid;
    }

    public String getNotetitle() {
        return notetitle;
    }

    public void setNotetitle(String notetitle) {
        this.notetitle = notetitle;
    }

    public String getNotedescription() {
        return notedescription;
    }

    public void setNotedescription(String notedescription) {
        this.notedescription = notedescription;
    }

    public String getAddeddate() {
        return addeddate;
    }

    public void setAddeddate(String addeddate) {
        this.addeddate = addeddate;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }

    @SerializedName("noteid")
    private float noteid;


    @SerializedName("notetitle")
    private String notetitle;

    @SerializedName("notedescription")
    private String notedescription;

    @SerializedName("addeddate")
    private String addeddate;

    @SerializedName("updateddate")
    private String updateddate;
}
