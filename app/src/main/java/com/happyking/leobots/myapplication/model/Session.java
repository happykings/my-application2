package com.happyking.leobots.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Session  {
    @SerializedName("mobile")
    @Expose

    private String mobile;

    @SerializedName("email")
    @Expose

    private String email;

    @SerializedName("Authorizations")
    @Expose

    private String Authorizations;

    @SerializedName("expiry_time")
    @Expose

    private long expiry_time;

    @SerializedName("refresh_token")
    @Expose

    private String refresh_token;

    @SerializedName("device_id")
    @Expose

    private String device_id;


    // Getter Methods

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getAuthorizations() {
        return Authorizations;
    }

    public long getExpiry_time() {
        return expiry_time;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getDevice_id() {
        return device_id;
    }

    // Setter Methods

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAuthorizations(String Authorizations) {
        this.Authorizations = Authorizations;
    }

    public void setExpiry_time(long expiry_time) {
        this.expiry_time = expiry_time;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}