package com.happyking.leobots.myapplication.network;

import android.content.Context;

import com.happyking.leobots.myapplication.model.GetNoteResponse;
import com.happyking.leobots.myapplication.model.LoginResponse;
import com.happyking.leobots.myapplication.model.AddNoteResponse;
import com.happyking.leobots.myapplication.util.SharedPrefUtil;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface ChipsyDemo {


    @POST("demoapis/app/user/login")
    @FormUrlEncoded
    Call<LoginResponse> login(@FieldMap Map<String,String> params);

    @POST("demoapis/app/user/register")
    @FormUrlEncoded
    Call<LoginResponse> register(@FieldMap Map <String,String>params);


    @POST("demoapis/app/notes/add-notes")
    @FormUrlEncoded
    Call<AddNoteResponse> addnote(@HeaderMap Map <String,String> params,@FieldMap Map <String,String>params1);



    @GET("demoapis/app/notes/get-notes")
    Call<GetNoteResponse> getnote(@HeaderMap Map <String,String> params);




}
