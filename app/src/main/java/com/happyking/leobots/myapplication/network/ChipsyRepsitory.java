package com.happyking.leobots.myapplication.network;

import androidx.lifecycle.MutableLiveData;

import com.happyking.leobots.myapplication.model.AddNoteResponse;
import com.happyking.leobots.myapplication.model.GetNoteResponse;
import com.happyking.leobots.myapplication.model.LoginResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChipsyRepsitory {
    private  static ChipsyRepsitory handler;
    private ChipsyDemo apiInterface;

    public static ChipsyRepsitory getInstance () {
        if (handler == null) {
            handler = new ChipsyRepsitory();
        }
        return handler;
    }
    public ChipsyRepsitory () {
        apiInterface = RetrofitServices.cteateService(ChipsyDemo.class);
    }

    public MutableLiveData<LoginResponse>  doLogin (Map<String, String> params,  MutableLiveData<LoginResponse> loginResponse   ) {

        apiInterface.login(params).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    loginResponse.setValue(response.body());
                    //loginResponse.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginResponse.setValue(null);
            }
        });
        return loginResponse;
    }

    public MutableLiveData<LoginResponse>  register (Map<String, String> params,  MutableLiveData<LoginResponse> loginResponse   ) {

        apiInterface.register(params).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    loginResponse.setValue(response.body());
                    //loginResponse.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginResponse.setValue(null);
            }
        });
        return loginResponse;
    }

    public MutableLiveData<AddNoteResponse> addnotes (Map<String,String> params, Map<String, String> params1, MutableLiveData<AddNoteResponse> noteResponse   ) {

        apiInterface.addnote(params,params1).enqueue(new Callback<AddNoteResponse>() {
            @Override
            public void onResponse(Call<AddNoteResponse> call, Response<AddNoteResponse> response) {
                if (response.isSuccessful()) {
                    noteResponse.setValue(response.body());
                    //loginResponse.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<AddNoteResponse> call, Throwable t) {
                noteResponse.setValue(null);
            }
        });
        return noteResponse;
    }

    public MutableLiveData<GetNoteResponse> getnotes (Map<String,String> params, MutableLiveData<GetNoteResponse> noteResponse   ) {

        apiInterface.getnote(params).enqueue(new Callback<GetNoteResponse>() {
            @Override
            public void onResponse(Call<GetNoteResponse> call, Response<GetNoteResponse> response) {
                if (response.isSuccessful()) {
                    noteResponse.setValue(response.body());
                    //loginResponse.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetNoteResponse> call, Throwable t) {
                noteResponse.setValue(null);
            }
        });
        return noteResponse;
    }
}
