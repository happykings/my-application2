package com.happyking.leobots.myapplication.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtil {


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "DemoTest";

    private static final String IS_LOGIN = "isLoggedIn";

    public static final String KEY_MOBILE = "mobile";

    public static final String KEY_EMAIL = "email";

    public static String getPrefName() {
        return PREF_NAME;
    }


    public boolean getIsLogin() {

        return pref.getBoolean(IS_LOGIN,false);
    }
    public  String getKeyMobile() {

        return pref.getString(KEY_MOBILE,"");
    }

    public  String getKeyEmail() {
        return pref.getString(KEY_EMAIL,"");
    }

    public  String getKey_device_id() {
        return pref.getString(Key_device_id,"");
    }

    public  String getKey_Authorizations() {
        return pref.getString(Key_Authorizations,"");
    }

    public  String getKey_refresh_token() {
        return pref.getString(Key_refresh_token,"");

    }

    private static final String Key_device_id = "device_id";

    private static final String Key_Authorizations = "authorizations";


    public static final String Key_refresh_token = "refresh_token";

    public SharedPrefUtil(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }



    public void createLoginSession(String mobile, String email,String authorizations,String refresh_token,String device_id){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(Key_Authorizations, authorizations);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);
        editor.putString(Key_device_id,device_id);
        editor.putString(Key_refresh_token,refresh_token);
        editor.putString(KEY_MOBILE,mobile);


        // commit changes
        editor.commit();
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity

    }





}

