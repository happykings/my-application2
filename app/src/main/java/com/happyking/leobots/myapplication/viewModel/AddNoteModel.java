package com.happyking.leobots.myapplication.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.happyking.leobots.myapplication.model.AddNoteResponse;
import com.happyking.leobots.myapplication.network.ChipsyRepsitory;

import java.util.Map;

public class AddNoteModel extends ViewModel {

    private MutableLiveData<AddNoteResponse> mutableLiveData;
    private ChipsyRepsitory chipsyNoteRepository;

    public void init () {
        if (mutableLiveData == null){
            mutableLiveData = new MutableLiveData<>();
        }
        chipsyNoteRepository = ChipsyRepsitory.getInstance();
    }

    public void addnotes(Map<String, String> params,Map<String, String> params1){
        mutableLiveData = chipsyNoteRepository.addnotes(params,params1, mutableLiveData);
    }



    public LiveData<AddNoteResponse> addNoteResponse() {
        return mutableLiveData;
    }
}
