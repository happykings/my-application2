package com.happyking.leobots.myapplication.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.happyking.leobots.myapplication.model.GetNoteResponse;


import com.happyking.leobots.myapplication.network.ChipsyRepsitory;

import java.util.Map;

public class GetNoteModel extends ViewModel {

    private MutableLiveData<GetNoteResponse> mutableLiveData;
    private ChipsyRepsitory chipsyNoteRepository;

    public void init () {
        if (mutableLiveData == null){
            mutableLiveData = new MutableLiveData<>();
        }
        chipsyNoteRepository = ChipsyRepsitory.getInstance();
    }

    public void getnotes(Map<String, String> params){
        mutableLiveData = chipsyNoteRepository.getnotes(params,mutableLiveData);
    }



    public LiveData<GetNoteResponse> getNoteResponse() {
        return mutableLiveData;
    }
}
