package com.happyking.leobots.myapplication.viewModel;

import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.happyking.leobots.myapplication.model.LoginResponse;
import com.happyking.leobots.myapplication.network.ChipsyRepsitory;

import java.util.Map;


public class LoginModel extends ViewModel {
    private MutableLiveData<LoginResponse> mutableLiveData;
    private ChipsyRepsitory chipsyRepository;

    public void init () {
        if (mutableLiveData == null){
            mutableLiveData = new MutableLiveData<>();
        }
        chipsyRepository = ChipsyRepsitory.getInstance();
    }

    public void doLogin(Map<String, String> params){
        mutableLiveData = chipsyRepository.doLogin(params, mutableLiveData);
    }
    public void register(Map<String, String> params){
        mutableLiveData = chipsyRepository.register(params, mutableLiveData);
    }


    public LiveData<LoginResponse> getLoginResponse() {
        return mutableLiveData;
    }
}
